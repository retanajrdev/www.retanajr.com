(function ($, window) {

    "use strict";
    var rjdev = rjdev || {};

    rjdev.site = {
      init: function(){
        var _this = this;
        _this.initResumeWorkExperienceAnimations();
      },

      /* Helper Functions here */
      initResumeWorkExperienceAnimations: function(){
        // Wait for DOM ready before creating animations
        $(function(){
          /* When mouse enters or exits the job experience row div */
          $('.resume-detail-full-width, .resume-tech-expertise-item').hover(function(){
            var resumeLogoImage = $(this).find('.resume-logo');

            /* Toggle the image-grayscale class for the image */
            resumeLogoImage.toggleClass('image-grayscale');
          });
        });
      }
    };

    //------ Initialize Functions-------
    rjdev.site.init();
}(jQuery, this));
