
(function ($, window) {

    "use strict";
    var rjdev = rjdev || {};

    rjdev.index = {
      init: function(){
        var _this = this;
      },

      calculateAge: function(birthday){ // birthday is a date
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
      }
    };

    //------ Initialize Functions-------
    rjdev.index.init();

    // Calculate age in the header
    $('#hero-content-age').text('"' + rjdev.index.calculateAge(new Date(1992,9,29)) + '"');
}(jQuery, this));
