# Set Up Instructions
## Front-End
### Sass
Install the latest version of Sass:

```
$ gem install sass
```

To confirm successful installation you should see the version number when running:

```
$ sass -v
```

Current is version: **sass-3.4.22**

**NOTE**: You may need to install Ruby before installing Sass if on Windows or Linux. Ruby comes by default
on Mac.


### Node.js and NPM
Install latest version of Node.js (currently using v4.4.7 LTS) and NPM (part of
Node.js installer) here https://nodejs.org/en/ .
Node.js and NPM should be installed on your computer (into /usr/local/ on Mac) after running the installer above.

Current version: Node.js **v4.4.7** and npm **v2.15.8**

 **Helpful links**:

 https://nodejs.org/en/

 https://docs.npmjs.com/getting-started/installing-node

### Bower ###
Install Bower via npm (if it doesnt work try installing as root/admin):

```
$ npm install -g bower
```

OR

```
$ sudo npm install -g bower
```

Current version is: **1.7.9**

**NOTE**: Later Bower will be used to install dependencies by using the bowser.json file.
### Zurb Foundation (Version 6)
Now that NPM and Bower are installed, use Bower to install Foundation files:

```
$ bower install foundation-sites --save
```

### MongoDB
Download latest version of MongoDB from https://www.mongodb.com/download-center#community and save it in a folder XYZ and extract it

MongoDB saves its data into /data/db by default so we need to create
that dir using sudo
```
# cd into mongodb folder that was extracted and cd into its bin folder
$ cd mongodb-osx-x86_64-3.2.8/
$ cd bin
```

Create /data/db/ folder with root and set its permissions
```
$ sudo mkdir -p /data/db
$ sudo chmod 777 /data
$ sudo chmod 777 /data/db
```

Now navigate back to the bin/ folder of the mongodb downdloaded folder and run mondod
```
$ cd mongodb-osx-x86_64-3.2.8/bin/
$ ./mongod
```

Notice that MongoDB is listening on port 27017 and is using dbpath of /data/db/...after verifying this kill the server with CTRL+C

Now we want to navigate into the mongodb bin/ folder and copy the bin/ files into our /usr/local/bin folder so we can use these programs globally
```
$ cd mongodb-osx-x86_64-3.2.8/bin/
$ sudo cp * /usr/local/bin
```

Verify the last step by running the which command for mongo and verify that it is using /usr/local/bin/mongo
```
$ which mongo
```
```
